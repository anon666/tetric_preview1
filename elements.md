## Elements

test

Elements list:

- brick
- mortar

### Element description

#### brick

props:

- name: brick
- description: clay bricks ?
- min_quantity: 1 // *one brick*
- ? integer: true // *integer count*
- flammable: false

#### mortar

props:

- name: mortar
- description: cement mortar ?
- min_quantity: 0.1 // *one brick*
- liquid: true
- ? integer: false // *integer count*
- flammable: false

