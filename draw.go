package main

import (
	"fmt"
	"github.com/gen2brain/raylib-go/raylib"
	_pf "tetric/pathfind"
)

// XXX should be called inside BeginDraw/EndDraw etc ?
// fixme ?
func DrawGridVol(gv *GridVol) {
	//fmt.Println("DrawGridVol gv:", gv)
	fmt.Println("DrawGridVol")

	//size := float32(gv.res)
	size := float32(1.0)
	//position := rl.NewVector3(0.0, 0.0, 0.0)

	//rl.DrawCube(position, size, size, size, rl.Red)
	//rl.DrawCubeWires(position, size, size, size, rl.Green)

	// Options
	drawSetBits := true
	_ = drawSetBits

	for x := 0; x < gv.sx; x++ {
		for y := 0; y < gv.sy; y++ {
			for z := 0; z < gv.sz; z++ {

				px := float32(x)
				py := float32(y)
				pz := float32(z)

				offset := size/2
			
				pos := rl.NewVector3(px + offset, py + offset, pz + offset)
				
				//rl.DrawCube(pos, size, size, size, rl.Green)
				rl.DrawCubeWires(pos, size, size, size, rl.Green)

				if drawSetBits {
					//v := gv.data[x][y][z]
	
					//v := gv.data[z][y][x]
					//v := gv.Get(x, y, z)

					// Rot90 / y-up ?
					// for rendering
					// fixme ?: not tested
					v := gv.data[x][z][y]

					if v > 0 {
						rl.DrawCube(pos, size, size, size, rl.SkyBlue)
					} 
				}
			}
		}
	}
}

func DrawPath(path _pf.Path) {
	fmt.Println("DrawPath")

	for i, n := range path {
		p(n)
		p(n.X, n.Y, n.Z)

		px := float32(n.X)
		py := float32(n.Y)
		pz := float32(n.Z)

		// ?
		cellSize := float32(1.0)

		if false {
			pos := rl.NewVector3(px - cellSize/2, py - cellSize/2, pz - cellSize/2)

			//size := float32(1.0)
			size := float32(0.3)
		
			rl.DrawCube(pos, size, size, size, rl.Black)
		}

		{
			if i > 0 {
				prevN := path[i-1]

				offset := cellSize/2

				prevPos := rl.NewVector3(
					float32(prevN.X) + offset,
					float32(prevN.Y) + offset,
					float32(prevN.Z) + offset)
				// CurPos
				pos := rl.NewVector3(px + offset, py + offset, pz + offset)

				rl.DrawLine3D(prevPos, pos, rl.Pink)

				DrawFatLine(prevPos, pos, 10, rl.Pink)
			}
		}

	}

	//panic(2)
}

// Util ?
func DrawFatLine(point1, point2 rl.Vector3, thickness int, color rl.Color) {

	for i := 0; i < thickness; i ++ {

		step := float32(0.02)
			
		v := step * float32(i)

		for j := 0;  j < 3; j++ {
			var ox rl.Vector3
			var oz rl.Vector3
			if j == 1 {
				ox = rl.NewVector3(v, 0, 0)
				oz = rl.NewVector3(-v, 0, 0)
			} else if j == 2 {
				ox = rl.NewVector3(0, v, 0)
				oz = rl.NewVector3(0, -v, 0)
			} else {
				ox = rl.NewVector3(0, 0, v)
				oz = rl.NewVector3(0, 0, -v)
			}

			p1a := rl.Vector3Add(point1, ox)
			p1b := rl.Vector3Add(point2, ox)

				
			rl.DrawLine3D(p1a, p1b, color)

			
			p2a := rl.Vector3Add(point2, oz)
			p2b := rl.Vector3Add(point2, oz)
				
			rl.DrawLine3D(p2a, p2b, color)

		}
	}
}

