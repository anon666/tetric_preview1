## Energy use diagram ##

test:

lathe:
- electric motor

cnc lathe:
- electric motor (main)
- stepper motors 2x (movement ?)

cnc cutter:
movement:
- 3x stepper motors
cutting tool:
- 1x electric motor
or
- 1x laser
or
- 1x plasma ?

cnc mill:
movement:
- 3x stepper motors
cutting tool:
- 1x electric motor


stone crusher:
- ?

black box:
- ? kw/h
- from specs ?
- from tests ?
- from evaluation ?

### Nodes ###

#### Description ####

? energy usage in kh/w

features:
- parametric ?
- irregular / shedule(d) use ?
  - ? use graph/program

#### Node types ####

- electric motor (parametric ?)
- stepper motor

### Other users ###

?

#### Building uses ####

- ? lighting
- ? heating
- ? services
- ? other

### Other ###

- ? non-electric energy use graph
  - ? for fuel based machinery etc
  - ? sankey diagram
