package main

import (
	"fmt"
	//glm "github.com/technohippy/go-glmatrix"
	//lm "github.com/xlab/linmath"
	. "github.com/go-gl/mathgl/mgl64"
	"github.com/gen2brain/raylib-go/raylib"
	
	_pf "tetric/pathfind"
	//. "tetric/math"
)

func main() {
	println("main()")

	////  XXX init ?
	
	//
	// Viz XXX separate ?
	//
	rl.SetConfigFlags(rl.FlagVsyncHint)
	rl.InitWindow(800, 450, "")

	rl.SetTargetFPS(60)

	if false {
		//pf := _pf.NewPathfinder(nil)
		pf := _pf.NewPathfinder()
		pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{10, 0, 0})
	}

	////
	sb := NewSbot()
	//b.pos = glm.Vec3FromValues(10, 20, 30)
	//sb.pos = glm.Vec3FromValues(0, 0, 0)
	//sb.SetPos(lm.Vec3{0, 0, 0})
	//sb.SetPos(mgl64.Vec3{0, 0, 0})
	sb.SetPos(Vec3{0, 0, 0})

	fmt.Println("sb.pos:", sb.Pos())

	sb.Spawn()

	//
	// GridVol
	//
	gv1 := NewGridVol(10, 10, 10, 1.0)
	_ = gv1

	gv1.Put(0, 0, 0, 1)
	gv1.Put(1, 1, 0, 1)
	gv1.Put(5, 5, 0, 1)

	//
	// GridVol pathfind
	//
	//var pf *_pf.Pathfinder
	var path _pf.Path
	//if true {
	{
		pf := _pf.NewPathfinderDataObj(gv1)
		gv1.Put(9, 0, 0, 1)
		//path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{10, 0, 0})
		//path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{10, 10, 0})
		//path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{5, 10, 0})
		//path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{10, 10, 10})

		path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{2, 2, 1})
		//path = pf.Pathfind(_pf.Pos{0, 0, 0}, _pf.Pos{5, 5, 5})
		//panic(2)
	}

	camera := rl.Camera3D{}
	camera.Position = rl.NewVector3(0.0, 10.0, 10.0)
	camera.Target = rl.NewVector3(0.0, 0.0, 0.0)
	camera.Up = rl.NewVector3(0.0, 1.0, 0.0)
	//camera.Fovy = 45.0
	camera.Fovy = 20.0
	//camera.Projection = rl.CameraPerspective
	camera.Projection = rl.CameraOrthographic

	cubePosition := rl.NewVector3(0.0, 0.0, 0.0)
	_ = cubePosition

	//shiftVec := glm.NewVec3()
	shiftAmt := 0.2

	//rl.BeginDrawing()
	//rl.BeginMode3D(camera)
	mesh := rl.GenMeshCube(1.0, 1.0, 8.0)
	model := rl.LoadModelFromMesh(mesh)
	//rl.EndMode3D()
	//rl.EndDrawing()

	//
	// Test
	//
	if true {
		//sb.MoveTo(Vec3{10, 0, 0}, 1.0)
		//sb.MoveTo(Vec3{10	, 0, 0}, 0.1)
		sb.MoveTo(Vec3{5, 0, 0}, 0.05)
		
		//sb.MoveTo(Vec3{10, 10, 0}, 0.1)
		
		//sb.MoveTo(Vec3{10, 10, 10}, 0.02)
	}


	orbitalCam := false
	// Set an orbital camera mode
	//rl.SetCameraMode(camera, rl.CameraOrbital)

	for !rl.WindowShouldClose() {
		// XXX input
		{
			// Toggle Camera
			if rl.IsKeyPressed(rl.KeySpace) {
				//rl.SetCameraMode(camera, 0)
				orbitalCam = !orbitalCam
				if orbitalCam {
					rl.SetCameraMode(camera, rl.CameraOrbital)
				} else {
					rl.SetCameraMode(camera, 0)
					camera.Position = rl.NewVector3(0.0, 10.0, 10.0)
				}
			}
		
			//pos := lm.Vec3{}
			newPos := sb.Pos()
		
			if rl.IsKeyDown(rl.KeyRight) {
				newPos[0] += shiftAmt
			}
			if rl.IsKeyDown(rl.KeyLeft) {
				newPos[0] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyUp) {
				//b.pos[1] -= shiftAmt
				newPos[2] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyDown) {
				//b.pos[1] += shiftAmt
				newPos[2] += shiftAmt		
			}

			// XXX update position
			sb.SetPos(newPos)
		}

		rl.UpdateCamera(&camera) // Update camera
	
		rl.BeginDrawing()

		rl.ClearBackground(rl.RayWhite)

		rl.BeginMode3D(camera)

		//rl.DrawCube(cubePosition, 2.0, 2.0, 2.0, rl.Red)
		//rl.DrawCubeWires(cubePosition, 2.0, 2.0, 2.0, rl.Maroon)
		if false {
			x := rl.NewVector3(float32(sb.Pos()[0]), float32(sb.Pos()[1]), float32(sb.Pos()[2]))
			rl.DrawCube(x, 2.0, 2.0, 2.0, rl.Red)
			rl.DrawCubeWires(x, 2.0, 2.0, 2.0, rl.Maroon)
		}

		if false {
			x := rl.NewVector3(float32(sb.Pos()[0]), float32(sb.Pos()[1]), float32(sb.Pos()[2]))
			
			//mesh := rl.GenMeshCube(1.0, 1.0, 1.0)
			//mesh := rl.GenMeshCube(1.0, 1.0, 8.0)
			//model := rl.LoadModelFromMesh(mesh)

			//rl.DrawModel(model, x, 1.0, rl.DarkBlue) // Draw 3d model
			rl.DrawModelWires(model, x, 1.0, rl.Green) // Draw 3d model
		}

		// Bot
		if true {
			//newPos := rl.NewVector3(float32(b.pos[0]), float32(b.pos[1]), float32(b.pos[2]))
			//b.pos = newPos

			// XXX Update bot ?
			sb.Update()

			// XXX Draw bot ?
			sb.view.Render()
		}

		// GridVol
		if true {
			DrawGridVol(gv1)
		}

		// PathFind
		if true {
			DrawPath(path)
		}

		rl.DrawGrid(10, 1.0)

		rl.EndMode3D()


		//rl.DrawText("Congrats! You created your first window!", 190, 200, 20, rl.LightGray)

		rl.EndDrawing()
	}

	rl.CloseWindow()
}
