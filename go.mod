module tetric

go 1.20

require (
	github.com/gen2brain/raylib-go/raylib v0.0.0-20230131162822-963c891c4eb9
	github.com/go-gl/mathgl v1.0.0
	golang.org/x/exp v0.0.0-20230202163644-54bba9f4231b
)

require golang.org/x/image v0.3.0 // indirect
