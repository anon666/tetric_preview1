list:

- [v] multiple tool configurations
- ? tool modifications / mod. configurations
- ? energy use diagram/graph
- ? precise / imprecise processes
- ? prime objects / objects graph
  - use better name ?
- ? assembly graph


***

### multiple tool configurations ###

a tool can have multiple configurations that extends base configuration
base configuration is needed for required tool operations
other configurations can be extensions to this

example:

- base press: (base configuration)
  - does pressing

- extended base press: (extended configuration)
  - includes base press
  - adds automatic lubrication pipes for easier forming
  - has custom name / tag


### precise / imprecise processes ###

- precise processes:
  - examples:
    - ? precise manufacturing
    - ? precise agriculture

- imprecise processes:
  - imprecise manufacturing:
    - examples:
      - ? metal hammer (ironwork)

### prime objects ###

? are objects that don't have any components or other objects
(subobjects)
? but usually can be parts of other objects, or used as parts to construct other
objects

example:

`
bricks, nails, wood planks etc
`

### prime objects -> objects graph ###

a graph of objects (or tree) that depend on each other
what they're made of

```text
wall <- brick(s)
     <- mortar
```

```text
wheel <- tire
```

```text
track (assembly) <- wheels
                 <- axles <- axle hubs(?) <- bearings
                 <- chains/tracks <- chain link

+ (bolt connectors / fastening)
+ (pads / sealings ?)
```

(like assembly diagram)

(? or exploded view)

### assembly graph ###

?
