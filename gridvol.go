package main

import (
	"fmt"
	"golang.org/x/exp/slices"
)

// TODO: ? add GridData for simple grid data storage
// TODO: ? add subvolumes
// TODO: ? add objects

// A voxel volume ?
type GridVol struct {
	//dx float64
	//dy float64
	//dz float64

	// Grid resolution ?
	res float64

	sx int
	sy int
	sz int
	
	// Voxel data / values ?
	data [][][]int
}

func NewGridVol(dx, dy, dz, res float64) *GridVol {
	fmt.Println("NewGridVol", dx, dy, dz, res)

	gv := &GridVol{
		sx: int(dx/res),
		sy: int(dy/res),
		sz: int(dz/res),
		res: res,
	}
	gv.generate()
		
	return gv
}

func (gv *GridVol) generate() {
	fmt.Println("GridVol generate")

	// XXX not checked
	// check the orientation / order
	// z-axis must be up/down

	r := make([][][]int, gv.sx)

	for x := 0 ; x < gv.sx ; x++ {
		r[x] = make([][]int, gv.sy)
		for y := 0; y < gv.sy; y++ { 
			r[x][y] = make([]int, gv.sz)
			for z := 0 ; z < gv.sz; z++ {
				//r[x][y][z] = 1
			}
		}
	}

	// Finally
	gv.data = r

	fmt.Println("done generate")
}

// Put value at pos
func (gv *GridVol) Put(px, py, pz int, val int) {
	fmt.Println("GridVol Put px:", px, "py:", py, "pz:", pz, "val:", val)
	
	gv.data[px][py][pz] = val
}

// Get value at pos
func (gv *GridVol) Get(px, py, pz int) int {
	fmt.Println("GridVol Get px:", px, "py:", py, "pz:", pz)	

	v := gv.data[px][py][pz]

	return v
}

// Fill with value
func (gv *GridVol) Fill(val int) {
	fmt.Println("GridVol Fill val:", val)
	
	// XXX order ? not checked
	// XXX fill from top-down (z-down) ?
	for z := 0; z < gv.sz; z++ {
		for y := 0; y < gv.sy; y++ {
			for x := 0; x < gv.sx; x++ {
				gv.data[x][y][z] = val
			}
		}
	}
}

func (gv *GridVol) Clear() {
	fmt.Println("GridVol Clear")
	
	gv.Fill(0)
}


func (gv *GridVol) GetGridData() [][][]int {
	fmt.Println("GridVol GetGridData")	

	// XXX generate data ?
	// XXX fixme: just create a new empty
	// slice of the same size ?
	data := slices.Clone(gv.data)

	// XXX order ? not checked
	// XXX fill from top-down (z-down) ?
	for z := 0; z < gv.sz; z++ {
		for y := 0; y < gv.sy; y++ {
			for x := 0; x < gv.sx; x++ {
				v := 0
				if !gv.isCellOpen(x, y ,z) {
					v = 1
				}
				data[x][y][z] = v
			}
		}
	}

	//return gv.data
	return data
}

// XXX internal ?
// XXX TODO: add isCellOpenF(func) ?
func (gv *GridVol) isCellOpen(x, y, z int) bool {
	return gv.data[x][y][z] == 0
}
