package main

import (
	"fmt"
	//glm "github.com/technohippy/go-glmatrix"
	//"github.com/gen2brain/raylib-go/raylib"
	. "github.com/go-gl/mathgl/mgl64"

	"tetric/transform"
	. "tetric/math"
)

////
////  Bot tasks / commands / operations:
////
//    list:
//		- ?pick object/item ?
//		- carry (object)
//		- drop object/item
//
//      - transfer object ?
//      - pass object ?
//      - put object (as part of something)
//      - install object ?
//      
//      - ? put_to
//      - ? take_from


// Simulation bot
type Sbot struct {
	// Implicit transform ?
	*transform.Transform
	//pos []float64

	// XXX move to mover ?
	moveSpeed float64
	dstPos Vec3

	// Cur task ?
	task string
	taskBuild bool

	view *SbotView
}

func NewSbot() *Sbot {
	fmt.Println("NewSbot")

	sb := &Sbot{
		Transform: transform.NewTransform(),
	}

	return sb
}

// Linear move to a position ?
func(sb *Sbot) MoveTo(pos Vec3, moveSpeed float64) {
	fmt.Println("Sbot MoveTo pos:", pos, "moveSpeed:", moveSpeed)

	sb.dstPos = pos
	sb.moveSpeed = moveSpeed

	var _ = `

	curPos := sb.Pos()
	newPos := Vec3MoveTowards(curPos, sb.dstPos, sb.moveSpeed)

	p(curPos)
	p(newPos)

	panic(curPos)
	`
}

// Path version
//func (sb *Sbot) GoToPath(pos []float64) {
//	fmt.Println("Sbot Goto pos:", pos)
//
//}

// Path version
func (sb *Sbot) MoveToPath(pos Vec3) {
	fmt.Println("Sbot MoveToPath pos:", pos)

	// Make path

	panic("not implemented")
}

// Adds sbotview to scene ?
func (sb *Sbot) Spawn() {
	fmt.Println("Sbot Spawn")

	sb.view = NewSbotView(sb)

	// XXX ? fixme ?
	sb.view.Load()
	
	sb.view.Spawn()
}

func (sb *Sbot) Update() {
	fmt.Println("Sbot Spawn")

	// XXX Update mover
	// XXX fixme: use move state(s)
	if sb.moveSpeed != 0 &&
		sb.Pos() != sb.dstPos {
		
		curPos := sb.Pos()
		newPos := Vec3MoveTowards(curPos, sb.dstPos, sb.moveSpeed)

		p(curPos)
		p(newPos)

		sb.SetPos(newPos)
	} else {
		// Move done, unset

		sb.dstPos = Vec3{}
		sb.moveSpeed = 0
	}
}
