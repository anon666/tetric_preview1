## Brick wall object construction ##

### Description ###

brick wall is a block material

### object material: ###

- #### brick wall:
  - bricks (90% ?)
  - mortar (10% ?)

### Methods ###

#### Constructed by bots ####

1. pick main materials (bricks? mortar?)
2. go to object/wall
3. put materials in place (?install / apply them (operation))
     - creates a delay (operation delay)


### Bot command list ###

1. reach place (test)
   ... test if place can be reached by bot
2. ...

#### Operation list/types ####

```
operation(add_bricks, n) = {
  delay(1000)
}

operation(add_mortar, n) = {
  // Apply time for mortar
  sleep(mortar_add_time)
}
```

#### Command list ####

```
1. go to nearby resource
2. take/pick resource
3. carry resource to the object
  - VIZ: (use pathfinding)
4. install material
  - use operation codes ?
  - use delays
  - use generic `transfer` method ?
5. ...
```

Analogy: openttd train transfer

### TODO ###

- test move bricks from one container to another (heap/pile)
- test put bricks into a wall object (install)

### Problems ###

- ? bulk objects
  - ? object containers
  - ? material containers
- ? carrying a single brick ?
  - or bulk...
- different types of clay bricks can be used ?
- ? take objects from piles / stocks
  - [v] object containers
- placement
  - ? object placement reach (in model; by bot)
  - ? precise placement
