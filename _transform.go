package main

import (
	//glm "github.com/technohippy/go-glmatrix"
	//"github.com/bloeys/gglm/gglm"
	lm "github.com/xlab/linmath"
	//"github.com/mojzesh/linmath"
	//"github.com/go-gl/mathgl"
)

type Transform struct {
	//pos [3]float64
	//rot [3]float64
	//pos *gglm.Vec3
	//rot *gglm.Vec3
	pos lm.Vec3
	rot lm.Vec3
	mat lm.Mat4x4
}

func NewTransform() *Transform {

	t := &Transform{
		//pos: glm.NewVec3(),
		//rot: glm.NewVec3(),
		//pos: gglm.NewVec3(0, 0, 0),
		//rot: gglm.NewVec3(0, 0, 0),
		pos: lm.Vec3{},
		rot: lm.Vec3{},
		mat: lm.Mat4x4{},
	}

	return t
}
