package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/piprate/json-gold/ld"
)

func main() {
	dir := "query/linkedql/steps/test-cases/"
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		fmt.Println(f.Name())
		if f.IsDir() {
			continue
		}
		path := dir + "/" + f.Name()
		data, err := ioutil.ReadFile(path)
		if err != nil {
			panic(err)
		}
		var a map[string]interface{}
		err = json.Unmarshal(data, &a)
		if err != nil {
			panic(err)
		}
		processor := ld.NewJsonLdProcessor()
		options := ld.NewJsonLdOptions("")
		context := map[string]interface{}{
			"@base":  "http://example.com/",
			"@vocab": "http://example.com/",
		}
		compact, err := processor.Compact(a["results"], context, options)
		if err != nil {
			panic(err)
		}
		a["results"] = compact
		data, err = json.Marshal(a)
		if err != nil {
			panic(err)
		}
		ioutil.WriteFile(path, data, f.Mode().Perm())
	}
}
