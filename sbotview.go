package main

import (
	"fmt"
	//glm "github.com/technohippy/go-glmatrix"
	"github.com/gen2brain/raylib-go/raylib"
	//github.com/beefsack/go-astar
)

type SbotView struct {
	m *Sbot

	// Model ?
	//mesh rl.Mesh
	model rl.Model

	spawned bool
}

func NewSbotView(m *Sbot) *SbotView {
	fmt.Println("NewBotView")

	v := &SbotView{
		m: m,
	}

	return v
}

// ?
func (v *SbotView) Load() {
	fmt.Println("BotView Load")

	//rl.BeginDrawing()

	fmt.Println("gen mesh...")
	//mesh := rl.GenMeshCube(1.0, 1.0, 1.0)
	//_ = mesh

	fmt.Println("load model...")
	//model := rl.LoadModelFromMesh(mesh)

	v.model = rl.LoadModelFromMesh(
		rl.GenMeshCube(1.0, 1.0, 1.0))
	
	//v.mesh = mesh
	//v.model = model

	fmt.Println("BotView done Load")

	//rl.EndDrawing()


	//panic(3)
}

// XXX add model to the scene ?
func (v *SbotView) Spawn() {
	fmt.Println("SbotView Spawn")

	if v.spawned {
		panic("already spawned ?")
	}

	// XXX fixme: check loaded ?
	//v.Load()

	v.spawned = true
}

func (v *SbotView) Render() {
	fmt.Println("SbotView Render")

	//v.Load()

	//dump(v.model)

	// XXX
	position := rl.NewVector3(float32(v.m.Pos()[0]), float32(v.m.Pos()[1]), float32(v.m.Pos()[2]))

	fmt.Println("position:", position)

	rl.DrawModel(v.model, position, 1.0, rl.Blue) // Draw 3d model
	//rl.DrawModelWires(v.model, position, 1.0, rl.Green) // Draw 3d model

	//panic(2)
}
