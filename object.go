package main

// World object / model ?

type Object struct {
	name string

	// Dimentions
	sx float64
	sy float64
	sz float64

	// ?
	// wiki:prime object
	Prime bool
}

func NewObject(sx, sy, sz float64, name string) *Object {
	ob := &Object{
		name: name,
		sx: sx,
		sy: sy,
		sz: sz,
	}

	return ob
}

// Put/install something to object ?
//  ...put something, like brick
func (ob *Object) Put(someting interface{}) {
}

// Get volume ?
func (ob *Object) Vol(amt float64) float64 {
	return ob.sx * ob.sy * ob.sz
}

// Has volume amt
func (ob *Object) HasEmptyVol(amt float64) {

}

// Find where volume can  be installed / placed ?
//  ...that bot can reach it (test)
func (ob *Object) FindEmptyVol(amt float64) [3]float64 {
	panic("not implemented")
}

