package pathfind

import (
	"fmt"
	"golang.org/x/exp/slices"
)

//
// Path
//
type Path []*Node

// Returns slice
func NewPath() Path {
	fmt.Println("NewPath")

	// Return a slice
	p := make(Path, 0)
	
	return p
}

func (p Path) Len() int {
	return len(p)
}

func (p Path) HasNode(n *Node) bool {
	for _, x := range p {
		if x.Cmp(n) {
			return true
		}
	}
	return false
}

//  Remove node from path (in-place)
func (p *Path) RemoveNode(n *Node) {
	idx := slices.IndexFunc(*p,
		func(e *Node) bool { return e.Cmp(n) } )
	if idx == -1 {
		panic("can't remove node")
	}

	(*p)[idx] = nil // For gc safety
	*p = slices.Delete(*p, idx, idx+1)
}

func (p Path) LastNode() *Node {
	return p[len(p)-1]
}

func (p *Path) PopLastNode() *Node {
	//x, a := (*p)[0], (*p)[1:]
	x, a := (*p)[len(*p)-1], (*p)[:len(*p)-1]
	*p = a
	return x
}
