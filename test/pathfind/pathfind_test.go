package pathfind

import (
	"fmt"
	"testing"
	//pf "pathfind"
)

func zTestPathfind(t *testing.T) {

	pf := NewPathfinder()

	if false {
		//p := pf.Pathfind(0, 0, 0, 1, 0, 0)
		//p := pf.Pathfind(pf.Pos{0, 0, 0}, pf.Pos{1, 0, 0})
		p := pf.Pathfind(Pos{0, 0, 0}, Pos{1, 0, 0})
		//p := pf.Pathfind(
		//	[3]int{0, 0, 0},
		//	[3]int{1, 0, 0})
		fmt.Println("Path:", p)

		//p2 := pf.Pathfind(
		//	0, 0, 0,
		//	5, 0, 0)
		p2 := pf.Pathfind(Pos{0, 0, 0}, Pos{5, 0, 0})
		fmt.Println("Path:", p2)

		p3 := pf.Pathfind(
			Pos{1, 1, 0},
			Pos{3, 3, 3})
		fmt.Println("Path:", p3)
	}

	//pp(game.field.h)
	//p4 := pathfind(CPos{0, 0}, CPos{-5, 0})
	//p4 := pf.Pathfind(pf.Pos{0, 0, 0}, pf.Pos{10, 0, 0})
	p4 := pf.Pathfind(Pos{0, 0, 0}, Pos{10, 0, 0})
	fmt.Println("Path:", p4)
}

func zTestPathfindData(t *testing.T) {

	pf := NewPathfinder()

	data := genArray(10, 10, 10)
	_ = data

	data[9][0][0] = 1

	p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{10, 0, 0}, data)
	//p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{10, 0, 0}, nil)
	//p1 := pf.PathfindData(Pos{10, 0, 0}, Pos{0, 0, 0}, nil)
	//p1 := pf.PathfindData(Pos{10, 0, 0}, Pos{0, 0, 0}, data)
	fmt.Println("Path:", p1)
}

func TestPathfindData(t *testing.T) {

	pf := NewPathfinder()

	data := genArray(10, 10, 10)
	_ = data

	//data[9][0][0] = 1

	p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{2, 0, 0}, nil)
	//p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{2, 2, 1}, data)
	//p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{10, 0, 0}, nil)
	//p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{10, 10, 10}, nil)
	//p1 := pf.PathfindData(Pos{0, 0, 0}, Pos{2, 2, 2}, nil)
	fmt.Println("Path:", p1)
}

