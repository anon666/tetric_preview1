package pathfind

// internal ?

func genArray(sx, sy, sz int) [][][]int {
	r := make([][][]int, sx)

	for x := 0 ; x < sx ; x++ {
		r[x] = make([][]int, sy)
		for y := 0; y < sy; y++ { 
			r[x][y] = make([]int, sz)
			for z := 0 ; z < sz; z++ {
				r[x][y][z] = 0
			}
		}
	}

	return r
}

// https://golangbyexample.com/two-dimensional-array-slice-golang/
func getArraySize(data [][][]int) (int, int, int) {
	sx := len(data)
	sy := len(data[0])
	sz := len(data[0][0])

	return sx, sy, sz
}
