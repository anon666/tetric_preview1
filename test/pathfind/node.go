package pathfind

import (
	"fmt"
)

//
// Node
//
type Node struct {
	X int
	Y int
	Z int
	Fs int
	Parent *Node
}

func NewNode(x, y, z int) *Node {
	return &Node{X: x, Y: y, Z: z}
}

func (n *Node) Cmp(oth *Node) bool {
	if n.X == oth.X && n.Y == oth.Y && n.Z == oth.Z {
		return true
	}
	
	return false
}

func (n *Node) String() string {
	return fmt.Sprintf("Node<%d, %d, %d fs=%d>", n.X, n.Y, n.Z, n.Fs)
}
