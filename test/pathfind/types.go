package pathfind

type Pos [3]int

type GridDataI interface {
	GetGridData() [][][]int
}
