package pathfind

// Info:
// https://github.com/BaijayantaRoy/Medium-Article/blob/master/A_Star.ipynb

// TODO:
// - ? Add / test different / proper heuristic; like euclidian distance
// - ? Write g-score also / test 

import (
	"fmt"
	//. "math"
	//"golang.org/x/exp/slices"
)

//
// Pathfinder
//
type Pathfinder struct {
	FscoreFunc func(a *Node, b *Node) int
	dataObj GridDataI
}

func NewPathfinder() *Pathfinder {
	fmt.Println("NewPathfinder")

	pf := &Pathfinder{
		// XXX, fixme ?
		FscoreFunc: _fscore_func,
		// XXX
		dataObj: nil,
	}

	return pf
}

func NewPathfinderDataObj(dataObj GridDataI) *Pathfinder {
	fmt.Println("NewPathfinderDataObj")

	pf := &Pathfinder{
		// XXX, fixme ?
		FscoreFunc: _fscore_func,
		dataObj: dataObj,
	}

	return pf
}


func (pf *Pathfinder) DataObj() GridDataI {
	return pf.dataObj
}

func (pf *Pathfinder) SetDataObj(obj GridDataI) {
	fmt.Println("[Pathfinder] SetDataObj:", obj)

	pf.dataObj = obj
}

func (pf *Pathfinder) Pathfind(s, g Pos) Path {
	fmt.Println("[Pathfinder] Pathfind", s, g)

	var data [][][]int
	// XXX
	data = nil

	// dataObj is set 
	if pf.dataObj != nil {
		data = pf.dataObj.GetGridData()
	}
	res := pf.PathfindData(s, g, data)

	return res
}

// XXX data can be nil ?
func (pf *Pathfinder) PathfindData(s, g Pos, data [][][]int) Path {
	fmt.Println("[Pathfinder] PathfindData", s, g)

	if data == nil {
		fmt.Println(" -> data is nil")
	}

	return pf.doPathfindData(s, g, data)
}

func (pf *Pathfinder) doPathfindData(s, g Pos, data [][][]int) Path {
	fmt.Println("[Pathfinder] doPathfindData", s, g)

	start := NewNode(s[0], s[1], s[2])
	goal := NewNode(g[0], g[1], g[2])
	cset := NewPath()
	oset := NewPath()
	path := NewPath()
	fscore := pf.FscoreFunc
	//field := game.n

	var sx, sy, sz int
	if data != nil {
		// Volume size
		sx = len(data)
		sy = len(data[0])
		sz = len(data[0][0])
	}

	fmt.Println("data sx:", sx, "sy:", sy, "sz:", sz)

	fmt.Println("FROM", start, "TO", goal)

	oset = append(oset, start)
	
	//pp(2)
	for len(oset) > 0 {
		// Current node
		curNode := _get_lowest_fscore_node(oset)
		if curNode.Cmp(goal) {
			println("GOAL")
			path = pf.tracebackPath(curNode)
			fmt.Println("PATH:", path)
			return path
		}

		//oset.remove(x)
		var _ = `
		for i, n := range oset {
			if n.Cmp(x) {
				oset[i] = nil // For gc safety
				oset = append(oset[:i], oset[i+1:]...)
				break
			}
		}
		`
		oset.RemoveNode(curNode)
		cset = append(cset, curNode)

		// XXX fixme ?
		children := NewPath()
		
		// Get positions
		for _, p := range pf.neighbours(curNode.X, curNode.Y, curNode.Z) {
			
			fmt.Println("-> p:", p)
			
			// Node position
			nodePosX := curNode.X + p[0]
			nodePosY := curNode.Y + p[1]
			nodePosZ := curNode.Z + p[2]
			
			// In bounds ?
			if nodePosX < 0 || nodePosY < 0 || nodePosZ < 0 {
				continue
			}

			/*
				// Check empty
				if !field.isCellOpen(y.x, y.y) {
					continue
				}
			*/
			if data != nil &&
				(nodePosX < sx && nodePosY < sy && nodePosZ < sz) {
				// 0 is open
				if data[nodePosX][nodePosY][nodePosZ] != 0 {
					//continue
				}
			}


			// Create new node
			// Child node
			childNode := NewNode(nodePosX, nodePosY, nodePosZ)

			fmt.Println("child node:", childNode)
		
			children = append(children, childNode)	
		}
		
		
		for _, childNode := range children {

			fmt.Println("-> child node:", childNode)

			// Child node in cset
			if cset.HasNode(childNode) {
				fmt.Println("node in cset")
				fmt.Println("cset:", cset)
				continue
			}

			//fmt.Println(field != nil, y.X, y.Y, y.Z)
			fmt.Println("->", childNode.X, childNode.Y, childNode.Z)

			childNode.Parent = curNode
			childNode.Fs += fscore(childNode, goal)
			// Child node not in oset
			childNodeInOset := false
			if oset.HasNode(childNode) {
				childNodeInOset = true
			}
			if !childNodeInOset {
				// Add child in oset
				oset = append(oset, childNode)
			}
		}
	}

	fmt.Println("No path found")
	
	return path
}

func (pf *Pathfinder) tracebackPath(n *Node) Path {
	//fmt.Println("[Pathfinder] tracebackPath n:", n)

	ret := make(Path, 0)
	node := n
	for node.Parent != nil {
		ret = append(ret, node)
		node = node.Parent
	}
	
	return ret
}

func (pf *Pathfinder) neighbours(x, y, z int) [][3]int {
	fmt.Println("[Pathfinder] neighbours x:", x, "y:", y, "z:", z)

	//r := make([]CPos, 7)
	r := [][3]int{
		[3]int{1, 0, 0},
		[3]int{0, 1, 0},
		[3]int{0, 0, 1},
		[3]int{-1, 0, 0},
		[3]int{0, -1, 0},
		[3]int{0, 0, -1},
	}

	return r
}

// XXX fixme ? don't pass data (although nil for data is possible)
// data can be nil (optional)
// nil can be passed as data
// the data itself is not  required, only the volume size is needed
//func (pf *Pathfinder) neighbours(x, y, z int, data [][][]int) [][3]int {
func (pf *Pathfinder) _neighbours(x, y, z int) [][3]int {
	fmt.Println("[Pathfinder] neighbours x:", x, "y:", y, "z:", z)

	//r := make([]CPos, 7)
	r := make([][3]int, 0)
	
	var _ = `
	var sx, sy, sz int
	if data != nil {
		// Volume size
		sx = len(data)
		sy = len(data[0])
		sz = len(data[0][0])
	}
	`

	//fmt.Println("data sx:", sx, "sy:", sy, "sz:", sz)

	next := func(x, y, z int) {
		fmt.Println("next",x,y,z)
	
		// Filter negative
		if x < 0 || y < 0 || z < 0 {
			//return
		}
		var _ = `
		// Filter bounds
		if data != nil {
			if x >=  sx || y >= sy || z >= sz {
				return
			}
		}
		`

		c := [3]int{x, y, z}
		fmt.Println("c:", c)
		r = append(r, c)
	}

	for i := 0; i < 3; i++ {

		z := i - 1
	
		next(x-1, y+1, z) // Top Left
		next(x, y+1, z)   // Top Center
		next(x+1, y+1, z) // Top Right

		next(x-1, y, z) // Center Left

		// Also add for z != 0
		if z != 0 {
			// Bottom/Top
			next(x, y, z) // Center Bottom/Top
		}

		next(x+1, y, z) // Center Right

		next(x-1, y-1, z) // Bottom Left
		next(x, y-1, z)   // Bottom Center
		next(x+1, y-1, z) // Bottom Right

	}

	fmt.Println("r:", r)
	//panic(2)

	return r
}


func _get_lowest_fscore_node(l Path) *Node {
	fmt.Println("_get_lowest_fscore_node l:", l)

	ret := l[0]
	for _, n := range l {
		if n.Fs < ret.Fs {
			ret = n
		}
	}
	
	fmt.Println("ret:", ret)
	//panic(2)
	
	return ret
}

func _fscore_func(a, b *Node) int {
	fmt.Println("_fscore_func a:", a, "b:", b)

	abs := func(x int) int {
		if x < 0 {
			return -x
		}
		return x
	}
	_ = abs

	//return abs(a.x-b.x) + abs(a.y-b.y) // Manhattan
	//return wrapCellDistX(a.x, b.x) + wrapCellDistY(a.y, b.y)

	//md := abs(a.X-b.X) + abs(a.Y-b.Y) // Manhattan
	md := abs(a.X-b.X) + abs(a.Y-b.Y) + abs(a.Z-b.Z)// ? Manhattan
	//wcd := wrapCellDistX(a.x, b.x) + wrapCellDistY(a.y, b.y)
	//p("dist:", md, wcd, "a:", a, "b:", b)
	fmt.Println("dist:", md)
	return md
	//return wcd
}
