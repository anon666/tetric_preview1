package math

import (
	//glm "github.com/technohippy/go-glmatrix"
	//"github.com/bloeys/gglm/gglm"
	//lm "github.com/xlab/linmath"
	//"github.com/mojzesh/linmath"
	. "github.com/go-gl/mathgl/mgl64"
)

var _ = `
// float32
func Lerp(a, b, t float32) float32 {
	return a + (b - a) * t
}
`

// float64
func Lerp(a, b, t float64) float64 {
	return a + (b - a) * t
}


func Vec3Lerp(a, b Vec3, t float64) Vec3 {
	return Vec3{
		Lerp(a[0], b[0], t),
		Lerp(a[1], b[1], t),
		Lerp(a[2], b[2], t)}
}

func Vec3DistanceTo(v, to Vec3) float64 {
	return to.Sub(v).Len()
}

func Vec3DivScalar(v Vec3, n float64) Vec3 {
	return Vec3{v[0] / n, v[1] / n, v[2] / n}
}

func Vec3MoveTowards(v, to Vec3, delta float64) Vec3 {
	vd := to.Sub(v)
	len := vd.Len()
	if len <= delta || len < Epsilon {
		return to
	} else {
		res := Vec3DivScalar(vd, len).Mul(delta)
		res = v.Add(res)
		return res
	}
}
